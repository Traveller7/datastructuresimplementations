package mx.edu.utr.datastructures.st1514;

import mx.edu.utr.datastructures.*;

/**
 *
 * @author Chiquito
 */
public class ArrayList implements List {

    private Object[] elements;
    private int size;

    public ArrayList() {
        this(10);
    }

    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    //It creates a new array with a length bigger and copies the values of the old array to the new array.
    public void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            int[] newArray;
            newArray = new int[newCapacity];
            System.arraycopy(elements, 0, newArray, 0, newCapacity);
        }
    }

    @Override
    //It inserts an element in a specific position in the Arraylist.
    public boolean add(Object element) {
        ensureCapacity(size++);
        elements[size++] = element;
        return true;
    }

    @Override
    //It is going to add a new value in specific cell.
    public void add(int index, Object element) {
        ensureCapacity(size++);
        for (int i = size; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    @Override
    //It is going to clear every value in the arrayList.
    public void clear() {
        for (int i = 0; i < elements.length; i++) {
            elements[i] = null;
            this.size = 0;
        }
    }

    @Override
    //This method is going to return an element of a specified position in the arrayList.
    public Object get(int index) {
        if (index >= this.size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        } else {
            for (int i = 0; i <= this.size; i++) {
                if (index == i - 1) {
                    elements[i] = index;
                }
            }
            return elements[index];
        }
    }

    @Override
    //This method will show the index of an specific value in the arrayList.
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    //It si going to return <tt>true</tt> if the arrayList doesn´t have any value.
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    //This method will remove an element by its index.
    public Object remove(int index) {
        CheckRange(index);
        Object oldElement = elements[index];

        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;
    }

    //As its name indicates... It will check the range of the array and if it is bigger that the size, it will give an exception.
    private void CheckRange(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        }
    }

    @Override
    //This method is going to replace an element in a specified position in the arrayList according with index.
    public Object set(int index, Object element) {
        Object old = elements[index];
        elements[index] = element;
        return old;
    }

    @Override
    //It returns the size of the arrayList.
    public int size() {
        return size;
    }
}